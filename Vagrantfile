# -*- mode: ruby -*-
# vi: set ft=ruby :

# Para generar una imagen virtual de 32 bits, se debe especificar la variable de
# ambiente de este proceso `ARCH` con valor `32`. En caso contrario, se utilizará
# una arquitectura base de 64 bits para la imagen virtual. Por ejemplo, los
# siguientes comandos crean imágenes de 32 y 64 bits, respectivamente:
# $ ARCH=32 vagrant up
# Por defecto, se usará la arquitectura del sistema base.
arch_32b = ENV.fetch("ARCH") { 1.size == 8 ? 64 : 32 }.to_i == 32
bits = arch_32b ? 32 : 64

# Configuración para la imagen virtual para Seminario de Lenguajes - Opción C.
Vagrant.configure("2") do |config|
  #
  # En caso de necesitar usar una arquitectura de 32 bits, invocar `vagrant up` de
  # la siguiente manera:
  # $ ARCH=32 vagrant up
  config.vm.box = arch_32b ? "bento/ubuntu-16.04-i386" : "bento/ubuntu-16.04"

  # Crea una red privada entre el sistema host (equipo físico) y la máquina virtual
  # para permitir acceder a ésta última.
  config.vm.network "private_network", ip: "10.100.10.10"

  # Crea un directorio compartido entre el host y la máquina virtual para poder
  # acceder al código desarrollado desde ambos sistemas.
  # Si se desea, se puede cambiar la ruta del directorio en el host para compartir
  # otro, modificando el valor "./codigo" por una ruta absoluta o relativa al
  # directorio donde se encuentra este archivo Vagrantfile.
  # Para mayor información sobre qué es una ruta relativa o absoluta, puede
  # consultarse este artículo de Wikipedia:
  # https://es.wikipedia.org/wiki/Ruta_(inform%C3%A1tica).
  # Desde la máquina virtual, la carpeta compartida será accesible desde el
  # directorio /codigo.
  config.vm.synced_folder "./codigo", "/codigo"

  # Define el nombre interno de la VM para el provider (VirtualBox).
  config.vm.define("contiki_adaptivesec_#{bits}") { |m| }

  # (IoT) El simulador Cooja cuenta con una interfaz gráfica, estas opciones
  # permiten abrir aplicaciones graficas instaladas en la máquina virtual
  # desde el host.
  config.ssh.forward_agent = true
  config.ssh.forward_x11 = true

  # Esta es la configuración de la máquina virtual que se creará en VirtualBox.
  # Nos permite modificar el hardware virtual y las características de la máquina.
  # Acá es donde se puede llegar a necesitar ajustar algún valor, dependiendo
  # del hardware físico sobre el cual correrá la máquina virtual.
  config.vm.provider "virtualbox" do |vb|
    # Nombre que se le dará a la máquina virtual en VirtualBox.
    # Si se desea crear más de una máquina virtual con este Vagrantfile, se debe
    # cambiar este atributo de la máquina para diferenciarlas.
    vb.name = "Contiki AdaptiveSec (#{bits} bits)"

    # Cantidad de memoria (en MiB) para la máquina virtual
    vb.memory = "256"
  end

  config.vm.provision "shell", path: "install-adaptivesec.sh"
end
