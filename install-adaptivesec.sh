#!/bin/bash
apt-get update -qq
# Opcionalmente, se puede descomentar la linea siguiente para actualizar el sistema:
# apt-get upgrade -y
apt-get -y install build-essential \
    gdb \
    valgrind \
    cppcheck \
    splint \
    vim \
    emacs \
    mcedit \
    manpages-dev

if [[ "$(arch)" =~ i[0-9]86 ]]; then
    arch='-i386'
else
    arch=''
fi

if [ "$(whoami)" = 'root' ] && [ -n "$SUDO_UID" ] ; then
    ruser=$SUDO_UID
    rgroup=$SUDO_GID
else
    ruser=$(id -r -u)
    rgroup=$(id -r -g)
fi

rhome=$(getent passwd $ruser | cut -d: -f6)

# Herramientas para IoT
apt-get -y install openjdk-8-jdk\
    git\
    ant\
    libncurses-dev\
    xauth
git clone --branch adaptivesec --recursive https://github.com/kkrentz/contiki.git /opt/contiki-adaptivesec
chown -R ${ruser}.${rgroup} /opt/contiki-adaptivesec
wget -nv -O- http://lihuen.linti.unlp.edu.ar/bajar/msp430/msp430${arch}.tar.xz | tar -xJf - -C /opt

if [ ! -e $rhome/.bashrc ]; then
    touch $rhome/.bashrc
    chown $ruser:$rgroup $rhome/.bashrc
fi

if [ "$(grep -ce '^export CONTIKI' $rhome/.bashrc)" -eq 0 ]; then
    echo "export PATH=/opt/msp430/bin:$PATH" >> $rhome/.bashrc
    echo "export CONTIKI=/opt/contiki-adaptivesec" >> $rhome/.bashrc
    echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> $rhome/.bashrc
fi
